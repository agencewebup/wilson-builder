<?php

namespace  builder\helpers;

class ModelMakerHelper
{
    public static function formatModelName($name)
    {
        if (substr($name, -1) == 's') {
            $name = substr($name, 0, -1);
        }
        $array = explode('_', $name);
        foreach ($array as $key => $value) {
            $array[$key] = ucfirst($value);
        }
        $name = implode($array);

        return $name;
    }
}
