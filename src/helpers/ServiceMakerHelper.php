<?php

namespace  builder\helpers;

class ServiceMakerHelper
{
    public static function formatServiceName($name)
    {
        if (substr($name, -1) != 's') {
            $name .= 's';
        }
        $array = explode('_', $name);
        foreach ($array as $key => $value) {
            $array[$key] = ucfirst($value);
        }
        $name = implode($array);

        return $name;
    }
}
