<?php
$this->startPrefix('builder')
    ->add('makeModels', '/makeModels', '\builder\controllers\ModelMaker.index')
    ->add('makeModels.checkDisponibility', '/makeModels/checkdisponibility', '\builder\controllers\ModelMaker.checkDisponibility')

    ->add('makeServices', '/makeServices', '\builder\controllers\ServiceMaker.index')
    ->add('makeServices.checkDisponibility', '/makeServices/checkdisponibility', '\builder\controllers\ServiceMaker.checkDisponibility')

    ->add('makePage', '/makePages', '\builder\controllers\PageMaker.index')
    ->add('makePage.createController', '/makePages/createController', '\builder\controllers\PageMaker.createController')
    ->setExceptionHandler('\builder\controllers\Errors');
