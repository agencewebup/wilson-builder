<?php

namespace builder\controllers;

class Controller extends \wilson\Controller
{
    protected function init()
    {
        parent::init();

        $this->view = new \water\views\Water();

        $htmlHelper = new \wilson\views\helpers\Html(
            $this->router,
            \wilson\Config::get('uri.assets'),
            \wilson\Config::get('uri.media')
        );

        $this->view->setHelper('Html', $htmlHelper);

        $this->view->init();

        $this->view->Html->addJs('../vendor/builder/webroot/javascript/main.js');

        $this->view->setLayout(\wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'vendor/webup/builder/src/templates/layouts/default.phtml');

        $this->view->setTemplate(\wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'vendor/webup/builder/src/templates/'.$this->className().'/'.$this->template.'.phtml');

        $this->view->set('webrootPath', \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'vendor/webup/builder/webroot/');
        $this->makeMenu();
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $this->view->setWaterVars(array(
            'headerTitle' => 'Wilson Builder',
            'headerUrl' => '',
            'logoutUrl' => '',
        ));
    }

    public function makeMenu()
    {
        $this->view->menu->add('Model Maker', $this->url(array('makeModels')), 'fa-users', 'modelMaker');
        $this->view->menu->add('Service Maker', $this->url(array('makeServices')), 'fa-calendar', 'serviceMaker');
        $this->view->menu->add('Page Maker', $this->url(array('makePage')), 'fa-cubes', 'pageMaker');
    }
}
