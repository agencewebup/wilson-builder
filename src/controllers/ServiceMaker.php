<?php

namespace builder\controllers;

class ServiceMaker extends Controller
{
    protected $servicesPath;
    protected $extension = '.php';

    public function init()
    {
        parent::init();
        $this->view->menu->setCurrent('serviceMaker');
        $this->servicesPath = \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'src/services/';
    }

    public function index()
    {
        $modelsFiles = array();
        $modelsPath = \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'src/models/';
        foreach (scandir($modelsPath) as $key => $file) {
            if ($file == '.' || $file == '..' || $file == 'empty') {
                continue;
            }
            if (is_file($modelsPath.$file)) {
                $modelName = str_replace('.php', '', $file);
                $modelsFiles[] = array('file' => $file , 'name' => $modelName,'model' => "\models\\".$modelName);
            }
        }

        $form = new \wilson\Form();

        if ($form->validate()) {
            $values = $form->getValues();
            foreach ($values as $value) {
                if (isset($value['create']) && $value['create'] == 1) {
                    $this->createServiceFile($value['text'], $value['model']);
                }
            }
            $this->createCrudService();
        }

        $this->view->set('form', $form);
        $this->view->set('modelsFiles', $modelsFiles);
    }

    public function checkDisponibility()
    {
        if (isset($_POST) && isset($_POST['name']) && !empty($_POST['name'])) {
            $this->json(array('name' => \builder\helpers\ServiceMakerHelper::formatServiceName($_POST['name']), 'dispo' => !$this->servicesFileExist($_POST['name'])));
        } else {
            $this->json(false);
        }
    }

    public function createCrudService()
    {
        $crudServiceFile = file_get_contents(\wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'vendor/webup/builder/src/fileTemplates/ServiceMaker/crud.phtml');
        file_put_contents($this->servicesPath."Crud.php", $crudServiceFile);
        chmod($this->servicesPath."Crud.php", 0666);
    }

    public function createServiceFile($serviceName, $modelName)
    {
        $serviceName = \builder\helpers\ServiceMakerHelper::formatServiceName($serviceName);

        //Récupération du contenu du fichier à créer
        ob_start();
        include \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'vendor/webup/builder/src/fileTemplates/ServiceMaker/serviceFile.phtml';
        $fileContent = ob_get_contents();
        ob_end_clean();

        //Création du fichier avec le contenu précedent
        $filename = $serviceName.$this->extension;
        $fullPath = $this->servicesPath.$filename;
        file_put_contents($fullPath, $fileContent);
        chmod($fullPath, 0666);
    }

    public function servicesFileExist($name)
    {
        $serviceName = \builder\helpers\ServiceMakerHelper::formatServiceName($name);

        if (file_exists($this->servicesPath.$serviceName.$this->extension)) {
            return true;
        } else {
            return false;
        }
    }
}
