<?php

namespace builder\controllers;

class ModelMaker extends Controller
{
    protected $modelsPath;
    protected $extension = '.php';

    public function init()
    {
        parent::init();
        $this->view->menu->setCurrent('modelMaker');
        $this->modelsPath = \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'src/models/';
    }

    public function index()
    {
        $db = \wilson\models\Database::getdb();
        $stmt = $db->execute('SHOW tables');
        $resultsTables = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $tables = array();
        foreach ($resultsTables as $resultsTable) {
            if ($resultsTable['Tables_in_db'] != "__federal") {
                $name = $resultsTable['Tables_in_db'];
                $stmt = $db->execute('DESCRIBE '.$name);
                $resultsProperties = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                $properties = array();
                foreach ($resultsProperties as $resultsPropertie) {
                    $resultsPropertie['Null'] = ($resultsPropertie['Null'] == "YES") ? true : false;
                    $resultsPropertie['SQLType'] = $resultsPropertie['Type'];
                    $resultsPropertie['Type'] = $this->propertyFormatType($resultsPropertie['Type']);
                    $properties[] = $resultsPropertie;
                }
                $tables[$name] = array('properties' => $properties);
            }
        }

        $form = new \wilson\Form();

        if ($form->validate()) {
            $values = $form->getValues();
            foreach ($values as $value) {
                if (isset($value['create']) && $value['create'] == 1) {
                    $this->createModelFile($value['text'], $value['index'], $tables[$value['index']]['properties']);
                }
            }
        }

        $this->view->set('form', $form);
        $this->view->set('tables', $tables);
    }

    public function checkDisponibility()
    {
        if (isset($_POST) && isset($_POST['name']) && !empty($_POST['name'])) {
            $name = \builder\helpers\ModelMakerHelper::formatModelName($_POST['name']);
            $this->json(array('name' => $name, 'dispo' => !$this->modelsFileExist($name)));
        } else {
            $this->json(false);
        }
    }

    public function createModelFile($modelname, $tablename, $properties)
    {
        $modelname = \builder\helpers\ModelMakerHelper::formatModelName($modelname);

        //Récupération du contenu du fichier à créer
        ob_start();
        include \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'vendor/webup/builder/src/fileTemplates/ModelMaker/modelFile.phtml';
        $fileContent = ob_get_contents();
        ob_end_clean();

        //Création du fichier avec le contenu précedent
        $filename = $modelname.$this->extension;
        $fullPath = $this->modelsPath.$filename;
        file_put_contents($fullPath, $fileContent);
        chmod($fullPath, 0666);
    }

    public function propertyFormatType($type)
    {
        //Suppression des parenthèse venant du type sql (ex : int(13) => int )
        $type = str_replace(' ', '', strtolower(preg_replace("/\([^)]+\)/", "", $type))); // 'ABC '

        //Tableau de correspondance typeSql => typePhp
        $conversion = array(
            'tinyint' => 'Integer',
            'smallint' => 'Integer',
            'mediumint' => 'Integer',
            'int' => 'Integer',
            'intunsigned' => 'Integer',
            'bigint' => 'Integer',
            'decimal' =>  'Float',
            'float' =>  'Float',
            'double' =>  'Float',
            'real' =>  'Float',
            'bit' => 'Integer',
            'boolean' => 'Integer',
            'char' => 'String' ,
            'varchar' => 'String',
            'tinytext' => 'String',
            'text' => 'String',
            'mediumtext' => 'String',
            'longtext' => 'String',
        );

        return  $conversion[$type];
    }

    public function modelsFileExist($name)
    {
        $modelname = \builder\helpers\ModelMakerHelper::formatModelName($name);
        if (file_exists($this->modelsPath.$modelname.$this->extension)) {
            return true;
        } else {
            return false;
        }
    }
}
