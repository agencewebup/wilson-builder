<?php

namespace builder\controllers;

class Errors extends Controller
{
    public function init()
    {
        parent::init();

        $exception = $this->args['exception'];

        if ($exception instanceof \wilson\ForbiddenException) {
            $this->error403();
        } elseif ($exception instanceof \wilson\NotFoundException) {
            $this->error404();
        } else {
            $this->error500();
        }
    }

    public function error403()
    {
        $this->response->status = 403;
        $this->view->setTemplate('../vendor/webup/builder/src/templates/Errors/error403.phtml');
    }

    public function error404()
    {
        $this->response->status = 404;
        $this->view->setTemplate('../vendor/webup/builder/src/templates/Errors/error404.phtml');
    }

    public function error500()
    {
        $this->response->status = 500;
        $this->view->setTemplate('../vendor/webup/builder/src/templates/Errors/error500.phtml');
        $this->view->set('exception', $this->args['exception']);
    }
}
