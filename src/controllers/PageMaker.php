<?php

namespace builder\controllers;

class PageMaker extends Controller
{
    protected $servicesPath;
    protected $modelsPath;
    protected $controllersPath;
    protected $routerFile;
    protected $adminControllerFile;
    protected $extension = '.php';
    protected $routes = array();

    public function init()
    {
        parent::init();
        $this->servicesPath = \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'src/services/';
        $this->modelsPath = \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'src/models/';
        $this->controllersPath = \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'src/admin/controllers/';
        $this->templatesPath = \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'src/admin/templates/';
        $this->routerFile = \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'src/routes.php';
        $this->adminControllerFile = \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'src/admin/controllers/Controller.php';
    }

    public function index()
    {
        $files = array();

        foreach (scandir($this->servicesPath) as $file) {
            if ($file == '.' || $file == '..' || $file == 'empty' || $file == "Crud.php") {
                continue;
            }
            if (is_file($this->servicesPath.$file)) {
                $serviceFile = str_replace('.php', '', $file);
                $modelFile = $this->getModelFromServiceFile($file);
                $files[] = array('modelFile' => str_replace(' ', '', $modelFile) , 'serviceFile' => str_replace(' ', '', $serviceFile));
            }
        }

        $this->view->set('files', $files);
    }

    public function createController()
    {
        $form = new \wilson\Form();

        //Get Model properties
        $modelProperties = array();
        if (is_file($this->modelsPath.$_GET['modelFile'].'.php')) {
            $file = fopen($this->modelsPath.$_GET['modelFile'].'.php', "r");
            while (!feof($file)) {
                $line = fgets($file);
                $pos = strpos($line, 'protected $');
                if ($pos) {
                    //Clean de la string
                    $line = str_replace('protected $', '', $line);
                    $line = str_replace(';', '', $line);
                    $line = str_replace(" ", '', $line);
                    $line = str_replace("\n", '', $line);
                    $line = str_replace("\r", '', $line);
                    $modelProperties[] = $line;
                }
            }
            fclose($file);
        }

        $tableTypes = array('text' => "Texte",
            'date' => "Date",
            'bool' => "Boolean",
            );

        $propertyTypes = array('text' => "Texte",
            'password' => "Password",
            'email' => "Email",
            'number' => "Nombre",
            'file' => "Fichier",
            'textarea' => "Textarea",
            'checkbox' => "Checkbox",
            'radio' => "Radio",
            'hidden' => "Hidden",
            'select' => "Select",
            );

        if ($form->validate()) {
            $values = $form->getValues();

            //Set les values pour les vues et le controlleur
            $data = array();
            $data['controllerName'] = (isset($values['fileName'])) ? $values['fileName'] : null;
            $data['serviceName'] = (isset($values['serviceName'])) ? $values['serviceName'] : null;
            $data['serviceVariable'] = 'service'.$data['serviceName'];
            $data['modelName'] = (isset($values['modelName'])) ? $values['modelName'] : null;
            $data['pageTitle'] = (isset($values['pageTitle'])) ? $values['pageTitle'] : null;
            $data['allFunction'] = (isset($values['allFunction'])) ? $values['allFunction'] : null;
            $data['allType'] = (isset($values['allType'])) ? $values['allType'] : null;
            $data['addButton'] = (isset($values['addButton'])) ? $values['addButton'] : null;
            $data['addButtonLabel'] = (isset($values['addButtonLabel'])) ? $values['addButtonLabel'] : 'Ajouter un élément';
            $data['editButton'] = (isset($values['editButton'])) ? $values['editButton'] : null;
            $data['deleteButton'] = (isset($values['deleteButton'])) ? $values['deleteButton'] : null;
            $data['addFunction'] = (isset($values['addFunction'])) ? $values['addFunction'] : null;
            $data['editFunction'] = (isset($values['editFunction'])) ? $values['editFunction'] : null;
            $data['deleteFunction'] = (isset($values['deleteFunction'])) ? $values['deleteFunction'] : null;
            $data['varName'] = strtolower($data['modelName']);
            $data['varsName'] = $data['varName'].'s';
            $data['modelProperties'] = array();
            $data['addProperties'] = array();
            $data['editProperties'] = array();
            if (isset($values['propertiesTable']) && $data['allType'] == 1) {
                foreach ($values['propertiesTable'] as $key => $value) {
                    if ($value['value'] == 1) {
                        $data['modelProperties'][] = array('type' => $value['type'], 'value' => $key);
                    }
                }
            }
            if (isset($values['propertiesGrid']) && $data['allType'] == 2) {
                $data['modelProperties'][] = $modelProperties[$values['propertiesGrid']['media']];
                $this->addRoute($data['controllerName'], 'sort', '/sort');
                $this->updateServiceGrid($data['serviceName']);
            }

            if (isset($values['propertiesCard']) && $data['allType'] == 3) {
                $data['modelProperties'][] = $modelProperties[$values['propertiesCard']['name']];
            }

            if (isset($values['propertiesAdd'])) {
                foreach ($values['propertiesAdd'] as $key => $value) {
                    if ($value['value'] == 1) {
                        $data['addProperties'][] = array('type' => $value['type'], 'value' => $key);
                    }
                }
            }
            if (isset($values['propertiesEdit'])) {
                foreach ($values['propertiesEdit'] as $key => $value) {
                    if ($value['value'] == 1) {
                        $data['editProperties'][] = array('type' => $value['type'], 'value' => $key);
                    }
                }
            }

            // Création du fichier controlleur
            $this->createFile($data['controllerName'], $this->controllersPath.$data['controllerName'].$this->extension, 'controllerFile.phtml', $data);

            if ($data['allFunction']) {
                //Création de la vue all
                $this->createFile($data['controllerName'], $this->templatesPath.$data['controllerName'].'/all.phtml', 'viewAll.phtml', $data);
                //Ajout de la route all
                $this->addRoute($data['controllerName'], 'all', '/');
                $this->generateMenu($data['controllerName']);
            }

            if ($data['addFunction']) {
                //Création de la vue add
                $this->createFile($data['controllerName'], $this->templatesPath.$data['controllerName'].'/add.phtml', 'viewAdd.phtml', $data);
                //Ajout de la route add
                $this->addRoute($data['controllerName'], 'add', '/add');
            }

            if ($data['editFunction']) {
                //Création de la vue edit
                $this->createFile($data['controllerName'], $this->templatesPath.$data['controllerName'].'/edit.phtml', 'viewEdit.phtml', $data);
                //Ajout de la route edit
                $this->addRoute($data['controllerName'], 'edit', '/edit/(?P<id>\d+)');
            }

            if ($data['deleteFunction']) {
                //Ajout de la route delete
                $this->addRoute($data['controllerName'], 'delete', '/delete/(?P<id>\d+)');
            }

            $this->generateRoutes($this->routes);
        }

        $this->view->set('form', $form);
        $this->view->set('types', $this->types());
        $this->view->set('modelProperties', $modelProperties);
        $this->view->set('propertyTypes', $propertyTypes);
        $this->view->set('tableTypes', $tableTypes);
    }

    public function addRoute($controllerName, $function, $url)
    {
        $this->routes[] = array('name' => strtolower($controllerName).'.'.$function,
                            'url' => '/'.strtolower($controllerName).$url,
                            'controller' => '\admin\controllers\\'.$controllerName.'.'.$function,
                            );
    }

    public function updateServiceGrid($name)
    {
        $file = $this->servicesPath.$name.'.php';
        $service = file($file);
        $find = false;
        foreach ($service as $line_num => $item) {
            if (strpos($item, 'public function sort') !== false || strpos($item, 'public function all') !== false) {
                return;
            }
            if (strpos($item, '}') !== false) {
                $find = $line_num;
            }
        }
        if ($find != false) {
            $test = array(  "\n\t".'public function sort($order)',
                            "\n\t".'{',
                            "\n\t\t".'foreach($order as $position => $objectId)',
                            "\n\t\t".'{',
                            "\n\t\t\t".'$object = $this->findById($objectId);',
                            "\n\t\t\t".'if($object){',
                            "\n\t\t\t\t".'$object->position($position);',
                            "\n\t\t\t\t".'$this->update($object);',
                            "\n\t\t\t".'}',
                            "\n\t\t".'}',
                            "\n\t".'}',
                            "\n",
                            "\n\t".'public function all()',
                            "\n\t".'{',
                            "\n\t\t".'return $this->db->all($this->model)->order(\'position ASC\');',
                            "\n\t".'}',
                            "\n",
            );

            array_splice($service, $line_num-1, 0, $test);
            //On vide le fichier
            file_put_contents($file, '');
            //On réécrit les routes =)
            foreach ($service as $key => $value) {
                file_put_contents($file, $value, FILE_APPEND);
            }
            chmod($file, 0666);
        } else {
            echo 'Impossible de modifier le service';
            die();
        }
    }

    public function generateMenu($name)
    {
        $find = false;
        $functionArray = array();
        //Lecture du fichier Controller.php existant
        $menu = file($this->adminControllerFile);
        foreach ($menu as $line_num => $item) {
            if (strpos($item, 'public function makeMenu(') !== false) {
                $find = $line_num;
            }
            if ($find != false) {
                $menu[$line_num] = str_replace(';', '', $item);
                $find = $line_num;
                //Si le menu existe déjà on stop
                if (strpos($item, strtolower($name).'.all') !== false) {
                    return;
                }
                if (strpos($item, '}') !== false) {
                    break;
                }
            }
        }
        $last_key = key(array_slice($menu, -1, 1, true));
        $line = "\t\t\t".'->add(\''.$name.'\', $this->url(array(\''.strtolower($name).'.all\')), \'fa-question\', \''.strtolower($name).'\');'."\n";
        array_splice($menu, $line_num, 0, array($line));
        //On vide le fichier
        file_put_contents($this->adminControllerFile, '');
        //On réécrit les routes =)
        foreach ($menu as $key => $value) {
            file_put_contents($this->adminControllerFile, $value, FILE_APPEND);
        }
    }

    public function generateRoutes($nextRoutes)
    {
        //On inverse le tableau ( Pour la clarté )
        $nextRoutes = array_reverse($nextRoutes, true);
        $routes = array();
        $index = null;
        //Lecture du fichier routes.php existant
        $router = file($this->routerFile);
        foreach ($router as $line_num => $route) {
            if (strpos($route, '$this->add(') !== false) {
                $index = 'web';
                $routes[$index] = array($route);
            } elseif (strpos($route, '$this->startPrefix(') !== false) {
                preg_match("/\('(\w+)'\)/", $route, $matches);
                $index = $matches[1];
                $routes[$index] = array($route);
            } else {
                $routes[$index][] = $route;
            }
        }

        //On ajoute les routes non existante dans le tableau
        if (isset($routes['admin'])) {
            $addNewLine = false;
            foreach ($nextRoutes as $key => $nextRoute) {
                $exist = false;
                foreach ($routes['admin'] as $key => $route) {
                    if (strpos($route, "'".$nextRoute['url']."'") !== false || strpos($route, "'".$nextRoute['name']."'") !== false) {
                        $exist = true;
                    }
                }
                if (!$exist) {
                    $addNewLine = true;
                    $line = "\t->add('".$nextRoute['name']."', '".rtrim($nextRoute['url'], '/')."', '".$nextRoute['controller']."')\n";
                    array_splice($routes['admin'], 2, 0, array($line));
                }
            }
            //On ajoute un saut de ligne pour grouper les routes vers un controller
            if ($addNewLine) {
                array_splice($routes['admin'], 2, 0, array("\n"));
            }
        } else {
            echo 'Le router ne contient pas de prefix admin';
        }

        //On vide le fichier
        file_put_contents($this->routerFile, '');
        //On réécrit les routes =)
        foreach ($routes as $key => $value) {
            file_put_contents($this->routerFile, $value, FILE_APPEND);
        }
    }

    public function createFile($folder, $path, $template, $data = array())
    {
        extract($data);

        if (!file_exists($this->templatesPath.DIRECTORY_SEPARATOR.$folder)) {
            mkdir($this->templatesPath.DIRECTORY_SEPARATOR.$folder, 0777);
        }

        ob_start();
        include \wilson\Config::get('path.root').DIRECTORY_SEPARATOR.'vendor/webup/builder/src/fileTemplates/PageMaker/'.$template;
        $fileContent = ob_get_contents();
        ob_end_clean();

        file_put_contents($path, $fileContent);
        chmod($path, 0666);
    }

    public function types()
    {
        return array(1 => 'Table',2 => 'Grid',3 => 'Card');
    }

    public function getModelFromServiceFile($serviceFile)
    {
        $file = fopen($this->servicesPath.$serviceFile, "r");
        while (!feof($file)) {
            $line = fgets($file);
            $pos = strpos($line, 'protected $model');
            if ($pos) {
                //Clean de la string
                $line = str_replace('protected $model = ', '', $line);
                $line = str_replace(';', '', $line);
                $line = str_replace("'", '', $line);
                $line = str_replace("\models\\", '', $line);

                return $line;
            }
        }
        fclose($file);

        return;
    }
}
