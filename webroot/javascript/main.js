(function($, window, document, undefined) {

    $(init);

    var displayers = $('input[data-show]');

    function init() {

        $('.js-checkbox').each(function(){
            hideOrDisplayDivForCheckbox(this);
        })

        $('.js-checkbox').on('change',function(){
            hideOrDisplayDivForCheckbox(this);
        });

        displayers.each(function(){
            dataShowChange(this);
        })
        displayers.on('change',function(){
            dataShowChange(this);
        })
    }

    function hideOrDisplayDivForCheckbox(checkbox){
        var element = $(checkbox).closest(".js-parent-div");
        element = $(element).find(".js-hidden-div");
        if($(checkbox).prop('checked')){
            element.show();
        }else{
            element.hide();
        }
    }

    function dataShowChange(object){
        if($(object).is(':checked')){
            $('.'+$(object).data('show')).show();
        }else{
            $('.'+$(object).data('show')).hide();
        }
    }


})(jQuery, window, document);
