(function($, window, document, undefined) {

    $(init);

    var allTypeDiv = $('.js-allType');
    var selectAllType = $('.js-display-allType');

    function init() {

        displayAllTypeDetail();
        selectAllType.on('change',function(){
            displayAllTypeDetail();
        })
    }

    function displayAllTypeDetail(){
        allTypeDiv.hide();
        var type = selectAllType.val();
        $('.js-type-'+type).show();
    }

})(jQuery, window, document);
