(function($, window, document, undefined) {

    $(init);

    var modelNames = $('.js-model-name');
    var submitButton = $('.js-submit-form');
    var form = $('.js-form');
    var notDispoClass = 'js-no-dispo';
    var fieldsetClass = 'js-fieldset';

    function init() {
        modelNames.each(function( index ) {
          checkNameDisponibility($(this));
        });
        modelNames.on('focusout',function(){
            checkNameDisponibility($(this));
        });

        submitButton.on('click',function(event){
            event.preventDefault();
            var notDispoNumber = 0 ;
            $('.'+notDispoClass).each(function( index ) {
                var checkbox = $($(this).closest('.js-fieldset')).find('.js-checkbox');
                if($(checkbox).is(':checked')){
                    notDispoNumber ++;
                }
            });
            var notDispoMessage = notDispoNumber
            notDispoMessage += (notDispoNumber > 1) ? " models seront supprimé" : " model serra supprimé";
            if(notDispoNumber > 0 ){
                if(confirm("Attention , "+notDispoMessage)){
                    form.submit();
                }
            }else{
                form.submit();
            }
        })
    }

    function checkNameDisponibility(object){
        var dataTable = object.data("table");
        var disponibilitySpan = $('.js-disponibility[data-table='+dataTable+']');
        $.ajax({
            url : '/builder/makeModels/checkdisponibility',
            type : 'POST',
            data : {
                name : object.val()
            },
            success : function(result, statut){ // code_html contient le HTML renvoyé
                object.val(result['name']);
                if(result['dispo']){
                    disponibilitySpan.html('<i class="fa fa-check"></i> Disponible !').css('color','green').removeClass(notDispoClass);
                }else{
                    disponibilitySpan.html('<i class="fa fa-times"></i> Pas disponible !').css('color','red').addClass(notDispoClass);
                }
            },
            error : function(){
                console.log('Erreur');
            }
        });
    }



})(jQuery, window, document);
